﻿using OnlineTest.Context;
using OnlineTest.Models;
using OnlineTest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineTest.Controllers
{
    public class SubjectController : Controller
    {
        ProjectDbContext db = new ProjectDbContext();
        // GET: Subject
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Add() {

            return View();
        }


        [HttpPost]
        public ActionResult Add(VmSubjectAdd model)
        {
            try {

                if (ModelState.IsValid)
                {
                    var data = new Subject
                    {
                        Id = model.Id,
                        Name = model.Name


                    };

                    db.Subjects.Add(data);
                    db.SaveChanges();
                }
            }
            catch (Exception ex) { 
                

            }

            return View();
        }
    }
}