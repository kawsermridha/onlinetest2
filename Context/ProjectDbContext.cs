﻿using OnlineTest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OnlineTest.Context
{
    public class ProjectDbContext : DbContext 
    {
        public ProjectDbContext() : base("name = projectconnectionstring")
        {

        }
        public DbSet<Subject> Subjects { get; set; }
    }
}