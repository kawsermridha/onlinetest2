﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineTest.ViewModels
{
    public class VmSubjectAdd
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Name Is Requierd")]
        public string Name { get; set; }
    }
}